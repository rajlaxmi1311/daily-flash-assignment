import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  bool isTapped = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Question5"),
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            setState(() {
              isTapped = true;
            });
          },
          child: Container(
            width: 300,
            height: 300,
            alignment: Alignment.center,
            color: isTapped
                ? const Color.fromARGB(255, 36, 123, 194)
                : const Color.fromARGB(255, 201, 35, 20),
            child: Text(isTapped ? "Container Tapped" : "Click Me"),
          ),
        ),
      ),
    );
  }
}
