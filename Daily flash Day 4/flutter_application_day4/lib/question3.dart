import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: SizedBox(
        width: 300,
        height: 100,
        child: FloatingActionButton(
            onPressed: () {},
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Rajlaxmi"),
                SizedBox(
                  width: 10,
                ),
                Icon(Icons.arrow_forward_ios_outlined)
              ],
            )),
      ),
    );
  }
}
