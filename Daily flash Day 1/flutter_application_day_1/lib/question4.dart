import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          //color: Colors.blue,
          // error will be shown when box color put outside the boxdecoration
          height: 300,
          width: 300,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.red), color: Colors.blue),
        ),
      ),
    );
  }
}
