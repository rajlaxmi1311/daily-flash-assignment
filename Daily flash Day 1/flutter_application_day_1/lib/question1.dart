import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //actionsIconTheme: [],
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        backgroundColor: Colors.blueGrey,
        actions: const [
          Icon(Icons.menu),
          SizedBox(
            width: 20,
          )
        ],
        centerTitle: true,
        title: const Text("Title at Center"),
      ),
      body: Center(
        child: Container(
            //final Widget? leading;

            ),
      ),
    );
  }
}
