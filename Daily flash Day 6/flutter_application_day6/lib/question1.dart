import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Question1"),
        backgroundColor: Colors.blueGrey,
      ),
      body: Column(children: [
        SizedBox(
            width: double.infinity,
            child: Image.asset(
              "assets/download.jpeg",
              height: 300,
              // width: 200,
              fit: BoxFit.cover,
            )),
        Container(
            margin: const EdgeInsets.all(10),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Burger",
                    style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.w700,
                    )),
                SizedBox(
                  height: 20,
                ),
                Text(
                    "A burger is a patty of ground beef grilled and placed between two halves of a bun.",
                    style: TextStyle(
                      color: Color.fromARGB(255, 90, 88, 88),
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ))
              ],
            ))
      ]),
    );
  }
}
