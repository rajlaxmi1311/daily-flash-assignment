import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Question4"),
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
      ),
      body: Center(
        child: Container(
          width: 300,
          height: 200,
          padding: const EdgeInsets.all(50),
          decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                    color: Color.fromARGB(255, 37, 190, 175),
                    offset: Offset(0, -50),
                    blurRadius: 8)
              ],
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20)),
              color: const Color.fromARGB(255, 221, 236, 112),
              border: Border.all(color: Colors.black)),
          child: const Text(
            "Rajlaxmi Pathade",
          ),
        ),
      ),
    );
  }
}
