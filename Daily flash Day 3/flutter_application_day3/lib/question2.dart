import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Container(
        height: 200,
        width: 200,
        alignment: Alignment.center,
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  "assets/image.png",
                ),
                fit: BoxFit.fill)),
        child: const Text(
          "Rajlaxmi",
          style:
              TextStyle(color: Colors.blueGrey, backgroundColor: Colors.white),
        ),
      )),
    );
  }
}
