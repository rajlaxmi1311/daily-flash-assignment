import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Image.asset("assets/image.png", height: 250, width: 250),
          const SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.all(30),
            decoration: BoxDecoration(
                color: const Color.fromARGB(255, 16, 149, 154),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromARGB(255, 14, 1, 16),
                      offset: Offset(0, 8),
                      blurRadius: 20),
                ],
                border: Border.all(color: const Color.fromARGB(255, 58, 3, 67)),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: const Text(
              "Rajlaxmi Pathade",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
            ),
          ),
          const SizedBox(height: 10),
        ]),
      ),
    );
  }
}
