import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  bool isTapped = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(children: [
          Image.network(
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTO_V4HjbEnDrneINTURZMCrs9O9vIF_aZ6g&s",
            height: 200,
            width: 200,
          ),
          const Spacer(),
          Container(
            height: 100,
            width: 200,
            color: Colors.red,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 100,
            width: 200,
            color: Colors.blue,
          ),
        ]),
      ),
    );
  }
}
