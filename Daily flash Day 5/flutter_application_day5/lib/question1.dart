import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile Information"),
        backgroundColor: const Color.fromARGB(255, 7, 100, 125),
        foregroundColor: Colors.white,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Image.asset("assets/image.png", height: 250, width: 250),
          const SizedBox(height: 20),
          const Text(
            "Username: Rajlaxmi Pathade",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 10),
          const Text(
            "Phone number : 9011701805",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ]),
      ),
    );
  }
}
